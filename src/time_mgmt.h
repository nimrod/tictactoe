#ifndef _H_TIME_
#define _H_TIME_

#include "types.h"

i64 moveTime(i64 time, i64 timePerMove, u8 numFree);

#endif